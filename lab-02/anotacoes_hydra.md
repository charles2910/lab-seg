# Anotações Hydra
Hydra é uma aplicação usada para ataques de força bruta contra
computadores. Ele é capaz de testar uma lista de senhas contra
um serviços e aplicações.

## Uso básico
Pode ser usado somente passando o endereço do servidor atacado e o
serviço a ser testado:

```bash
hydra attack.server.example service
```

- O endereço pode ser um nome dns ou ip (v4 ou v6).
- O serviço pode ser `ssh`, `ftp`, `smb`, `pop3`, etc.

Geralmente, também é passada uma lista com as senhas usando a opção
`-P path/to/list`, mas também é possível usar a opção `-x min:max:charset`
para gerar uma lista. Também é possível definir uma porta customizada com
a opção `-s PORT`, e usar uma lista de usernames também com `-L path/to/file`. 

Para acelarar a execução do ataque de força bruta, é possível usar mais de
uma _thread_ para executar o programa com `-t N_TASKS`, e pedir para parar
a execução após encontrar uma senha válida com `-f`.
