# Laboratório II – Uso de Ferramentas (Hydra e Medusa) - Relatório
## Alunos
- Carlos Henrique Lima Melara 9805380
- Caio Marcos Chaves Viana 11208217

## Introdução
Neste laboratório, iremos aprender como usar duas ferramentas de
ataque de força bruta. As ferramentas em questão são hydra e medusa.
Ambas necessitam de indicações de dicionários com nomes de usuário e
senhas para realizar a descoberta de máquinas vulneráveis (i.e. que
tenham uma combinação de usuário/senha pertencente às listas que
utilizamos).

Essas ferramentas podem ser muito utéis para auditar máquinas e
serviços em rede, e este é exatamente o caso de uso pressuposto
neste laboratório.

## Realizando a auditoria

### Selecionando dicionários
Como foi dito anteriormente, é necessário passar uma lista de senhas
e/ou nomes de usuário para a ferramenta realizar a busca. O hydra
possibilita a geração de senhas de forma automática, contudo é muito
melhor utilizar dicionários (i.e. listas pré-montadas) com as senhas
mais comumente utilizadas - a taxa de acerto e a velocidade são bem
maiores.

Dessa forma, é necessário encontrar primeiramente uma boa lista de
senhas, e uma ótima fonte é o Kali Linux. Ele possui uma grande
quantidade de dicionários sempre atualizados e bastante extensos.
Duas listas foram baixadas [deste repositório](https://github.com/3ndG4me/KaliLists),
elas são dicionários fornecidos pelo pacote metasploit. Uma delas,
`piata_ssh_userpass.txt`, contém combinações de usuários e senhas.
Já a outra, `password.lst`, possui somente senhas.
Além disso, uma lista foi baixada com termos brasileiros e senhas que 
foram encontrados em vazamentos envolvendo dados brasileiros. A lista tem o nome de `br-wordlist`
se encontra [nesse repositório](https://github.com/mmatje/br-wordlist)

### Usando o Hydra
O alvo selecionado para a auditoria foi o servidor `gisa dot icmc dot
usp dot br` por ter um serviço ssh antigo e exposto. Dessa forma, foi
executados os seguinte comando:

```bash
hydra -f -C lists/ssh_userpass.txt -t 64 gisa.icmc.usp.br ssh | tee gisa_hydra_ssh_list
hydra -f -l root -P lists/password.lst -t 64 gisa.icmc.usp.br ssh | tee gisa_hydra_password_list
```

### Usando o Medusa
Assim como o alvo do Hydra, o mesmo host foi selecionado para o medusa (`gisa dot icmc dot
usp dot br`) pelos motivos já citados, e também por estar exposto na internet, 
o que simula com mais veracidade uma tentativa de invasão.

Para utilizar o medusa, podem ser utilizadas diversas opções, como lista para nome de hosts, 
lista para nomes de usuários, lista de senhas, paralelização, entre outros comandos.

O comando em questão a ser utilizado foi:

```bash
medusa -h 143.107.183.110 -u root -P br-wordlist.txt -M ssh -t 10 | tee output_medusa
```

Nesse caso, houve a tentativa de atacar apenas o usuário root, com a wordlist 
`br-wordlist` no serviço ssh e com 10 tentativas de login sendo feitas simultâneamente. 

O arquivo de output tem o nome de `output_medusa.txt` e pode ser encontrado seguindo esse [link](output_medusa.txt).<br/>
Como pode ser visto no log do comando, não houve login realizado com sucesso, entretanto 
o ataque só foi realizado contra o usuário root. Como pontos de melhoria visando um ataque 
mais próximo da realidade, e maior chance de sucesso de invasão, poderia ser utilizada uma wordlist
para usuários também, como feito no ataque usando o hydra, além de se tentar outras wordlists.

## Conclusão (Hydra x Medusa)
Como pudemos observar, ambas as ferramentas possuem parâmetros intuitivos e relativamente simples, 
além de que o tempo de resposta foi similar em ambas (com o Hydra sendo um pouco mais rápido). 
Entretanto, a ferramenta Hydra já tenta realizar o ataque de bruteforce de maneira paralela, 
enquanto a ferramenta Medusa necessita de um parâmetro extra. Além disso, a ferramenta Hydra 
recebe suporte até hoje, enquanto a ferramenta Medusa foi descontinuada. Portanto o grupo 
considera o Hydra uma ferramenta melhor.
