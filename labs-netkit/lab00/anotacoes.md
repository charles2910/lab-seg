# Adaptação e Execução do Laboratório Introdução ao Netkit para o Kathará

## Alterações Iniciais para Utilizar o Kathará

Apesar do kathará ter bastante compatibilidade com o netkit, algumas coisas
precisam ser alteradas. Por exemplo, o kathará não permite que os nomes das
máquinas sejam em letras maiúsculas, então é necessário alterar o arquivo
`lab.conf` e o nome das pastas que serão usadas pelas máquinas para letras
minúsculas - o diff entre o lab original e a versão atualizada pode ser
vista abaixo. Feitas essas alterações, já é possível iniciar o laboratório
utilizando o kathará (`kathara lstart`). Também removeu-se o arquivo `lab.dep`
por não estar sendo utilizado e adicionou-se a imagem docker a ser usada por
cada máquina.

```diff
diff --git a/lab00/CLIENTE.startup b/lab00/cliente.startup
similarity index 100%
rename from lab00/CLIENTE.startup
rename to lab00/cliente.startup
diff --git a/lab00/lab.conf b/lab00/lab.conf
index 66df81c..8fed9b8 100755
--- a/lab00/lab.conf
+++ b/lab00/lab.conf
@@ -1,22 +1,20 @@
-#Introducao ao LAB
+# Introduçao ao LAB
 LAB_DESCRIPTION='Laboratorio Zero - Introducao ao Linux e ao netkit'
-LAB_VERSION='1.0'
-LAB_AUTHOR='Paulo Henrique Moreira Gurgel'
+LAB_VERSION='2.0'
+LAB_AUTHOR='Paulo Henrique Moreira Gurgel e Carlos Henrique Lima Melara'
 LAB_EMAIL='paulogur@grad.icmc.usp.br'
 LAB_WEB='http://www.grad.icmc.usp.br/~paulogur/netkit.php'
 
-#Ligacao das maquinas nos dominios de colisao
-#Todas as maquinas terao interfaces (eth0) ligadas ao hub virtual (HUB1)
-CLIENTE[0]=HUB1
-SERVIDOR[0]=HUB1
+# Ligação das máquinas nos domínios de colisão
+# Todas as máquinas terão interfaces (eth0) ligadas ao hub virtual (HUB1)
+cliente[0]=HUB1
+servidor[0]=HUB1
 
-#Definicao da quantidade de memoria disponivel para cada host. 
-#Este lab requer pouco mais de 128Mb de RAM para ser executado
-CLIENTE[mem]=32
-SERVIDOR[mem]=32
+# Definição da quantidade de memória disponível para cada host.
+# Este lab requer pouco mais de 128Mb de RAM para ser executado
+cliente[mem]=32
+servidor[mem]=32
 
-#Cria os cowfiles na pasta temp e nunca mais teremos problemas com o perfil XD
-CLIENTE[filesystem]=/tmp/L0CLI.disk
-SERVIDOR[filesystem]=/tmp/L0SVR.disk
-
-#Havera inicializacao paralela pela presenca do lab.dep
+#Definicao da imagem docker usada pelas máquinas.
+cliente[image]="lsec/netkit-lsec:debian10"
+servidor[image]="lsec/netkit-lsec:debian10"
diff --git a/lab00/lab.dep b/lab00/lab.dep
deleted file mode 100755
index e69de29..0000000
diff --git a/lab00/SERVIDOR.startup b/lab00/servidor.startup
similarity index 100%
rename from lab00/SERVIDOR.startup
rename to lab00/servidor.startup
diff --git a/lab00/SERVIDOR/root/contas.ods b/lab00/servidor/root/contas.ods
similarity index 100%
rename from lab00/SERVIDOR/root/contas.ods
rename to lab00/servidor/root/contas.ods
diff --git a/lab00/SERVIDOR/root/myfile.doc b/lab00/servidor/root/myfile.doc
similarity index 100%
rename from lab00/SERVIDOR/root/myfile.doc
rename to lab00/servidor/root/myfile.doc
```


## Alterações nos Passos do Roteiro

Como estamos usando o kathará e várias aplicações foram atualizadas, foi
necessário adaptar alguns passos do roteiro original. As alterações estão
indicadas abaixo pelo seu respectivo número no roteiro original.

### Passo 3
Pode ser interessante alterar o nome do tar utilizado.

### Passo 4
A inicialização do laboratório pelo kathará é realizado com o seguinte comando:

```bash
kathara lstart
```

Assumindo que o diretório atual é o que contém o laboratório - mais
especificamente, o arquivo lab.conf. Caso contrário, pode usar a opção `-d` e
especificar o caminho do diretório contendo o lab.

Portanto, seria interessante criar um novo passo anterior que mude para
o diretório do laboratório e então execute a inicialização do lab:

```bash
cd lab00
```

```bash
kathara lstart
```

### Passo 5
O kathara permite opcionalmente a conexão com a máquina virtualizada através
do comando `kathara connect` - funciona da mesma forma que uma conexão ssh.
A vantagem é que permite usar o shell padrão do(a) usuário(a) e facilita a
visualização. É preciso de uma janela do terminal para cada comando.

```bash
kathara connect cliente
```

```bash
kathara connect servidor
```

### Passo 6
O kathará vem com o mapeamento `/hosthome` desativado por padrão. Então aqui há
uma escolha a ser feita, pedir para alterar o arquivo `~/.config/kathara.conf`
e alterar a opção `hosthome_mount` para `true`, ou fazer o dump em `/shared`.
Nesse caso, os arquivos são colocados no diretório `shared` dentro do diretório
do lab. Aqui optei por fazer em `/shared`.

```bash
tcpdump -i eth0 -v -n -s 1600 -w /shared/lab00.pcap
```

Acho que a melhor opção é usar a pasta `shared` na maioria dos casos porque já
vem habilitada por padrão e facilita a organização dos arquivos, mantendo-os
contidos em seu respectivo lab.

### Passo 9
Trocar `/hosthome` por `/shared` e `home do usuário logado` por `shared do
laboratório`.

### Passo 10
Trocar `em sua pasta home` para `na pasta shared`.

### Passo 11
O kathara coloca o usuário na raiz do sistema por padrão, então a saída
do comando `pwd` será `/`.

### Passo 12
Trocar `hosthome` por `shared` - na dica também!

### Passo 22
Não é necessário (nem existe o comando) executar o `halt` no kathará.

### Passo 23
Para encerrar o lab, basta se desconectar das máquinas com:

```bash
exit
```

E depois executar:

```bash
kathara lclean
```

Se não estiver no diretório do lab, é possível indicar o caminho com a
opção `-d`.

### Passo 24
Ele não é necessário com o kathará.
