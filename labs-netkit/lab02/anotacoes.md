# Adaptação e Execução do Laboratório Uma rede simples conectada por um hub para o Kathará

## Alterações Iniciais para Utilizar o Kathará

Apesar do kathará ter bastante compatibilidade com o netkit, algumas coisas
precisam ser alteradas. Por exemplo, o kathará não permite que os nomes das
máquinas sejam em letras maiúsculas, então é necessário alterar o arquivo
`lab.conf`, `lab.dep` e o nome das pastas que serão usadas pelas máquinas para
letras minúsculas - o diff entre o lab original e a versão atualizada pode ser
vista abaixo. Feitas essas alterações, já é possível iniciar o laboratório
utilizando o kathará (`kathara lstart`). Também adicionou-se a imagem docker
a ser usada por cada máquina.

```diff
diff --git a/lab02/HOST1.startup b/lab02/host1.startup
similarity index 100%
rename from lab02/HOST1.startup
rename to lab02/host1.startup
diff --git a/lab02/HOST1/etc/network/interfaces b/lab02/host1/etc/network/interfaces
similarity index 100%
rename from lab02/HOST1/etc/network/interfaces
rename to lab02/host1/etc/network/interfaces
diff --git a/lab02/HOST2.startup b/lab02/host2.startup
similarity index 100%
rename from lab02/HOST2.startup
rename to lab02/host2.startup
diff --git a/lab02/HOST3.startup b/lab02/host3.startup
similarity index 100%
rename from lab02/HOST3.startup
rename to lab02/host3.startup
diff --git a/lab02/HOST4.startup b/lab02/host4.startup
similarity index 100%
rename from lab02/HOST4.startup
rename to lab02/host4.startup
diff --git a/lab02/lab.conf b/lab02/lab.conf
index 3e1ec2d..2d6c8b0 100755
--- a/lab02/lab.conf
+++ b/lab02/lab.conf
@@ -1,34 +1,32 @@
 #Introducao ao LAB
 LAB_DESCRIPTION='Laboratorio II - Nossa rede ganhou um switch'
-LAB_VERSION='1.0'
-LAB_AUTHOR='Paulo Henrique Moreira Gurgel'
-LAB_EMAIL='paulogur@grad.icmc.usp.br'
-LAB_WEB='http://www.grad.icmc.usp.br/~paulogur/netkit.php'
+LAB_VERSION='2.0'
+LAB_AUTHOR='Paulo Henrique Moreira Gurgel e Carlos Henrique Lima Melara'
+LAB_EMAIL='lsec@icmc.usp.br'
+LAB_WEB='https://www.lsec.icmc.usp.br/livronetkitbr/'
 
 #Ligacao das maquinas nos dominios de colisao
 #Todas as maquinas terao interfaces (eth0) ligadas ao hub virtual (HUB1)
-HOST1[0]=CaboAzul
-HOST2[0]=CaboVermelho
-HOST3[0]=CaboVerde
-HOST4[0]=CaboAmarelo
-SW1[0]=CaboAzul
-SW1[1]=CaboVermelho
-SW1[2]=CaboVerde
-SW1[3]=CaboAmarelo
+host1[0]=CaboAzul
+host2[0]=CaboVermelho
+host3[0]=CaboVerde
+host4[0]=CaboAmarelo
+sw1[0]=CaboAzul
+sw1[1]=CaboVermelho
+sw1[2]=CaboVerde
+sw1[3]=CaboAmarelo
 
 #Definicao da quantidade de memoria disponivel para cada host. 
 #Este lab requer pouco mais de 160Mb de RAM para ser executado
-HOST1[mem]=48
-HOST2[mem]=32
-HOST3[mem]=32
-HOST4[mem]=32
-SW1[mem]=32
+host1[mem]=48
+host2[mem]=32
+host3[mem]=32
+host4[mem]=32
+sw1[mem]=32
 
-#Configuracao dos filesystems
-HOST1[filesystem]=/tmp/L2H1.disk
-HOST2[filesystem]=/tmp/L2H2.disk
-HOST3[filesystem]=/tmp/L2H3.disk
-HOST4[filesystem]=/tmp/L2H4.disk
-SW1[filesystem]=/tmp/L2SW1.disk
-
-#Havera inicializacao paralela pela presenca do lab.dep
+# Definição da imagem docker usada pelas máquinas
+host1[image]="lsec/netkit-lsec:debian10"
+host2[image]="lsec/netkit-lsec:debian10"
+host3[image]="lsec/netkit-lsec:debian10"
+host4[image]="lsec/netkit-lsec:debian10"
+sw1[image]="lsec/netkit-lsec:debian10"
diff --git a/lab02/lab.dep b/lab02/lab.dep
index 3cb7d88..c4e6859 100755
--- a/lab02/lab.dep
+++ b/lab02/lab.dep
@@ -1,5 +1,4 @@
-HOST1: SW1
-HOST3: HOST1
-HOST2: HOST1
-HOST4: HOST1
-
+host1: sw1
+host3: host1
+host2: host1
+host4: host1
diff --git a/lab02/SW1.startup b/lab02/sw1.startup
similarity index 100%
rename from lab02/SW1.startup
rename to lab02/sw1.startup
diff --git a/lab02/SW1/root/switch-me.sh b/lab02/sw1/root/switch-me.sh
similarity index 100%
rename from lab02/SW1/root/switch-me.sh
rename to lab02/sw1/root/switch-me.sh
```


## Alterações nos Passos do Roteiro

Como estamos usando o kathará e várias aplicações foram atualizadas, foi
necessário adaptar alguns passos do roteiro original. As alterações estão
indicadas abaixo pelo seu respectivo número no roteiro original.

### Passo 2
Pode ser interessante alterar o nome do tar utilizado para
`kathara_lab02.tar.gz` ao invés de `netkit_lab02.tar.gz`.

### Passo 3
A inicialização do laboratório pelo kathará é realizado com o seguinte comando:

```bash
kathara lstart
```

Assumindo que o diretório atual é o que contém o laboratório - mais
especificamente, o arquivo lab.conf. Caso contrário, pode usar a opção `-d` e
especificar o caminho do diretório contendo o lab.

Portanto, seria interessante criar um novo passo anterior que mude para
o diretório do laboratório e então execute a inicialização do lab:

```bash
cd lab01
```

```bash
kathara lstart
```

### Passo 4
O comando `vlist` foi substituído por `kathara list`, contudo ele não
está funcionando completamente. É possível listar os containers em execução
com `sudo kathara list --all`.

Eu vou abrir um bug no repositório do kathará para averiguar a situação.

### Passos 5 e 6
O kathara permite opcionalmente a conexão com a máquina virtualizada através
do comando `kathara connect` - funciona da mesma forma que uma conexão ssh.
A vantagem é que permite usar o shell padrão do(a) usuário(a) e facilita a
visualização. É preciso de uma janela do terminal para cada comando.

```bash
kathara connect hostX
```

Ademais o comando `ifconfig` está obsoleto. Ele ainda vem instalado por
padrão no Debian 10, mas não deve vir no Debian 11. Toda a suíte `net-tools`,
`arp`, `ifconfig`, `route`, `netstat`, `ipmaddr`, ... está obsoleta. Então
deve-se pensar em migrar os roteiros para a ferramenta `iproute2` que tem
uma sintaxe parecida, mas não igual.

#### Tabela de equivalência
| Program   | Obsoleted by |
|-----------|--------------|
| arp       | ip neighbor  |
| ifconfig  | ip address   |
| ipmaddr   | ip maddress  |
| iptunnel  | ip tunnel    |
| route     | ip route     |
| nameif    | ifrename     |
| mii-tool  | ethtool      |

Portanto podemos executar `ip address` ao invés de `ifconfig`.

### Passo 7 e 9
Trocar `hosthome` por `shared`.

### Passo 8
Adicionar opção `-v` para mostrar o número de pacotes capturados para os
alunos.

### Passo 10
A rede configurada é `192.168.1.0/24`, mas o texto fala em `192.168.0.0/24`. É
preciso trocar um deles.

### Passo 13
Trocar referências a `hosthome` por `shared` e também trocar a
explicação.

### Passo 15 (extra)
Para encerrar o lab, basta se desconectar das máquinas com:

```bash
exit
```

E depois executar:

```bash
kathara lclean
```

Se não estiver no diretório do lab, é possível indicar o caminho com a
opção `-d`.

### Aprendendo um pouco mais sobre linux
Trocar referências `lab2` por `lab02` e `lab2b` por `lab02b`.

Trocar caminho `/lab2/SW1/root/switch-me.sh` por `lab02/sw1/root/switch-me.sh`.

#### Passo 6
Trocar para `ip address add 0.0.0.0 dev eth0`.

#### Passo 7
Trocar para `ip address add 0.0.0.0 dev eth1`.

#### Passo 8
Trocar para `ip address add 0.0.0.0 dev eth2`.

#### Passo 9
Trocar para `ip address add 0.0.0.0 dev eth3`.

#### Passo 10
Trocar para `ip address add 192.168.1.10/24 dev br0 > Configura a interface com
o ip especificado e a máscara de rede correta`.

#### Passo 11
adicionar `ip link set br0 up > Finalmente ... se um switch.`, a explicação é a
mesma do passo 9 antigo.
