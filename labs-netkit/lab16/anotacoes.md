# Adaptação e Execução do Laboratório Detecção de Intrusão com SNORT para o Kathará


## Alterações Iniciais para Utilizar o Kathará

Apesar do kathará ter bastante compatibilidade com o netkit, algumas coisas
precisam ser alteradas. Por exemplo, o kathará não permite que os nomes das
máquinas sejam em letras maiúsculas, então é necessário alterar o arquivo
`lab.conf`, `lab.dep` e o nome das pastas que serão usadas pelas máquinas para
letras minúsculas - o diff entre o lab original e a versão atualizada pode ser
vista abaixo. Feitas essas alterações, já é possível iniciar o laboratório
utilizando o kathará (`kathara lstart`).

```diff
diff --git a/lab16/EMPRESA1.startup b/lab16/empresa1.startup
similarity index 100%
rename from lab16/EMPRESA1.startup
rename to lab16/empresa1.startup
diff --git a/lab16/EMPRESA2.startup b/lab16/empresa2.startup
similarity index 100%
rename from lab16/EMPRESA2.startup
rename to lab16/empresa2.startup
diff --git a/lab16/EMPRESA3.startup b/lab16/empresa3.startup
similarity index 100%
rename from lab16/EMPRESA3.startup
rename to lab16/empresa3.startup
diff --git a/lab16/EMPRESA3/etc/proftpd/proftpd.conf b/lab16/empresa3/etc/proftpd/proftpd.conf
similarity index 100%
rename from lab16/EMPRESA3/etc/proftpd/proftpd.conf
rename to lab16/empresa3/etc/proftpd/proftpd.conf
diff --git a/lab16/INTERNET.startup b/lab16/internet.startup
similarity index 100%
rename from lab16/INTERNET.startup
rename to lab16/internet.startup
diff --git a/lab16/lab.conf b/lab16/lab.conf
index 8227fac..a877ad9 100644
--- a/lab16/lab.conf
+++ b/lab16/lab.conf
@@ -1,34 +1,24 @@
 #Introducao ao LAB
-LAB_DESCRIPTION='Laboratorio XVI - Snort'
-LAB_VERSION='1.0'
-LAB_AUTHOR='Paulo Henrique Moreira Gurgel'
-LAB_EMAIL='paulogur@grad.icmc.usp.br'
-LAB_WEB='http://www.paulogurgel.com.br/netkit.php'
+LAB_DESCRIPTION="Laboratorio XVI - Snort"
+LAB_VERSION="1.0"
+LAB_AUTHOR="Paulo Henrique Moreira Gurgel"
+LAB_EMAIL="paulogur@grad.icmc.usp.br"
+LAB_WEB="http://www.paulogurgel.com.br/netkit.php"
 
 #Ligacao das maquinas nos dominios de colisao
-
-EMPRESA1[0]=HUB1
-EMPRESA2[0]=HUB1
-EMPRESA3[0]=HUB1
-SERVIDOR[0]=HUB1
-SERVIDOR[1]=MODEM
-ROTEADOR[1]=MODEM
-ROTEADOR[0]=CLOUD
-INTERNET[0]=CLOUD
+empresa1[0]="HUB1"
+empresa2[0]="HUB1"
+empresa3[0]="HUB1"
+servidor[0]="HUB1"
+servidor[1]="MODEM"
+roteador[1]="MODEM"
+roteador[0]="CLOUD"
+internet[0]="CLOUD"
 
 #Definicao da quantidade de memoria disponivel para cada host. 
-EMPRESA1[mem]=64
-EMPRESA2[mem]=96
-EMPRESA3[mem]=96
-SERVIDOR[mem]=256
-ROTEADOR[mem]=64
-INTERNET[mem]=128
-
-#Localização dos arquivos .disk
-EMPRESA1[filesystem]=/tmp/L16E1.disk
-EMPRESA2[filesystem]=/tmp/L16E2.disk
-EMPRESA3[filesystem]=/tmp/L16E3.disk
-SERVIDOR[filesystem]=/tmp/L16SV.disk
-ROTEADOR[filesystem]=/tmp/L16RT.disk
-INTERNET[filesystem]=/tmp/L16IN.disk
-
+empresa1[mem]="64"
+empresa2[mem]="96"
+empresa3[mem]="96"
+servidor[mem]="256"
+roteador[mem]="64"
+internet[mem]="128"
diff --git a/lab16/lab.dep b/lab16/lab.dep
index 8c0851a..793a368 100644
--- a/lab16/lab.dep
+++ b/lab16/lab.dep
@@ -1,5 +1,5 @@
-INTERNET: ROTEADOR
-SERVIDOR: ROTEADOR
-EMPRESA1: SERVIDOR
-EMPRESA2: SERVIDOR
-EMPRESA3: SERVIDOR
+internet: roteador
+servidor: roteador
+empresa1: servidor
+empresa2: servidor
+empresa3: servidor
diff --git a/lab16/ROTEADOR.startup b/lab16/roteador.startup
similarity index 100%
rename from lab16/ROTEADOR.startup
rename to lab16/roteador.startup
diff --git a/lab16/SERVIDOR.startup b/lab16/servidor.startup
similarity index 100%
rename from lab16/SERVIDOR.startup
rename to lab16/servidor.startup
diff --git a/lab16/SERVIDOR/etc/proftpd/proftpd.conf b/lab16/servidor/etc/proftpd/proftpd.conf
similarity index 100%
rename from lab16/SERVIDOR/etc/proftpd/proftpd.conf
rename to lab16/servidor/etc/proftpd/proftpd.conf
diff --git a/lab16/SERVIDOR/etc/snort/snort.conf b/lab16/servidor/etc/snort/snort.conf
similarity index 100%
rename from lab16/SERVIDOR/etc/snort/snort.conf
rename to lab16/servidor/etc/snort/snort.conf
diff --git a/lab16/SERVIDOR/root/snort_lab.conf b/lab16/servidor/root/snort_lab.conf
similarity index 100%
rename from lab16/SERVIDOR/root/snort_lab.conf
rename to lab16/servidor/root/snort_lab.conf
diff --git a/lab16/SERVIDOR/root/snort_original.conf b/lab16/servidor/root/snort_original.conf
similarity index 100%
rename from lab16/SERVIDOR/root/snort_original.conf
rename to lab16/servidor/root/snort_original.conf
```


## Alterações Gerais no Roteiro

O antigo netkit era executado em uma máquina virtual específica e uma sistema
de arquivos híbridos para permitir a execução de aplicações dentro dos
ambientes simulados. Já o kathará utiliza containers para o ambiente simulado,
baseando-se em versões mais atualizadas das aplicações. Contudo, a máquina base
provida pelo kathará vem somente com os pacotes mais básicos do Debian 10, por
isso é necessário estender a imagem (construir uma nova imagem baseada na
atualmente fornecida) para abarcar as seguintes aplicações:

- snort;
- proftpd (`proftpd-basic`);
- ftp;
- ettercap (`ettercap-graphical`);

Como foi necessário contruir uma nova imagem baseada na fornecida pelo kathará,
é preciso indicar no arquivo de configuração que se deve buscar essa imagem
específica:

```diff
diff --git a/lab16/lab.conf b/lab16/lab.conf
index a877ad9..d0eeafa 100644
--- a/lab16/lab.conf
+++ b/lab16/lab.conf
@@ -22,3 +22,11 @@ empresa3[mem]="96"
 servidor[mem]="256"
 roteador[mem]="64"
 internet[mem]="128"
+
+#Definicao da imagem docker usada pelas máquinas.
+empresa1[image]="netkit/netkit:debian10"
+empresa2[image]="netkit/netkit:debian10"
+empresa3[image]="netkit/netkit:debian10"
+servidor[image]="netkit/netkit:debian10"
+roteador[image]="netkit/netkit:debian10"
+internet[image]="netkit/netkit:debian10"
```

Outro problema enfrentado foi a configuração das contas de usuário. Por padrão,
as máquinas do ambiente vinham com as contas `comprador`, `joaquim`, `manuel` e
`maria` pré-configuradas. Entretanto, com a atualização das aplicações do
sistema, não é mais possível fazer login com a senha pré-estabelecida -
provavelmente por uma alteração na forma de criar e armazenar as novas senhas.
Para contornar essa situação, a criação das contas foi refeita a partir do
respectivo arquivo `shared.startup`, o qual contém comando que são executados
por todas as máquinas ao iniciar o ambiente. Basicamente, é necessário alterar
o mecanismo da geração de senha. Hoje em dia, é necessário já passar como
argumento as senhas encriptadas para o `useradd`. O exemplo das alterações se
encontra abaixo:

```diff
diff --git a/lab16/shared.startup b/lab16/shared.startup
index 5d24365..0f65529 100644
--- a/lab16/shared.startup
+++ b/lab16/shared.startup
@@ -1,12 +1,10 @@
 echo "Voce esta no computador $HOSTNAME" > /etc/skel/local_$HOSTNAME.txt
-useradd -d /home/manuel -m -p 123man manuel
-useradd -d /home/maria -m -p 123mar maria
-useradd -d /home/joaquim -m -p 123joa joaquim
-useradd -d /home/comprador -m -p 123com comprador
+useradd -d /home/manuel -m -p $(openssl passwd -1 123man) manuel
+useradd -d /home/maria -m -p $(openssl passwd -1 123mar) maria
+useradd -d /home/joaquim -m -p $(openssl passwd -1 123joa) joaquim
+useradd -d /home/comprador -m -p $(openssl passwd -1 123com) comprador
 echo "Pasta do manuel" > /home/manuel/manuel.txt
 echo "Pasta da maria" > /home/maria/maria.txt
 echo "Pasta do joaquim" > /home/joaquim/joaquim.txt
 echo "Pasta do comprador" > /home/comprador/comprador.txt
 chmod 777 /home/* -R
-rm /etc/shadow -f
-mv /etc/shadow.new /etc/shadow
```

Por fim, foi necessário adicionar o endereço da máquina `empresa3` ao seu
próprio arquivo `/etc/hosts` para que o `proftpd` funcionasse corretamente:

```diff
diff --git a/lab16/empresa3.startup b/lab16/empresa3.startup
index f507298..d1f528f 100644
--- a/lab16/empresa3.startup
+++ b/lab16/empresa3.startup
@@ -1,3 +1,4 @@
 #configuracao do host EMPRESA3
 ifconfig eth0 10.1.1.13 netmask 255.0.0.0 up
 route add default gw 10.1.1.1 dev eth0
+echo 10.1.1.13 EMPRESA3 >> /etc/hosts
```

Um último problema surgiu ao se tentar executar o `ettercap` para fazer o Man
in the Middle. Essa ferramenta desativa as opçãos de encaminhamento de pacotes
do kernel (ipv4 e ipv6 *forwarding*) para ela própria fazer essa função.
Contudo, como estamos utilizando um contâiner, não é possível fazer essas
alterações de dentro do próprio contâiner. Dessa forma, o ettercap falha e não
é possível realizar o ataque. Algumas ~~gambiarras~~ alternativas foram
cogitadas, mas seria necessário conversar e discutir mais pois elas demandariam
bastante tempo para serem realizadas.


## Alterações nos Passos do Roteiro

Como estamos usando o kathará e várias aplicações foram atualizadas, foi
necessário adaptar alguns passos do roteiro original. As alterações estão
indicadas abaixo pelo seu respectivo número no roteiro original.

### Passo 3
A inicialização do laboratório pelo kathará é realizado com o seguinte comando:

```bash
kathara lstart
```

Assumindo que o diretório atual é o que contém o laboratório - mais
especificamente, o arquivo lab.conf. Caso contrário, pode usar a opção `-d` e
especificar o caminho do diretório contendo o lab.

### Passo 15
A nova versão do snort não permite que se use `any` na declaração de `HOME_NET`
por causa do seguinte erro:

```bash
+++++++++++++++++++++++++++++++++++++++++++++++++++
Initializing rule chains...
WARNING: /etc/snort/rules/dos.rules(42) threshold (in rule) is deprecated; use detection_filter instead.

ERROR: /etc/snort/rules/community-virus.rules(19) !any is not allowed: !$DNS_SERVERS.
Fatal Error, Quitting..
```

Uma alternativa é declarar ambos - `HOME_NET` e `EXTERNAL_NET` - como `0.0.0.0/0`.

### Passo 18
Como foi dito anteriormente, não é possível executar o ettercap atualmente no
kathará. Então esse passo não pode ser executado.

### Passo 20
Na atual sintaxe do snort, é necessário fazer a seguinte alteração em `snort.conf`:

```diff
--- /tmp/snort.conf	2022-12-16 22:36:18.863807677 +0000
+++ /etc/snort/snort.conf	2022-12-16 22:36:39.563754462 +0000
@@ -1,7 +1,7 @@
 #Configurações da rede
 #####################################
 var HOME_NET 10.0.0.0/8
-var EXTERNAL_NET !HOME_NET
+var EXTERNAL_NET !$HOME_NET
 #####################################
 
 var DNS_SERVERS $HOME_NET
```

Para evitar o erro:

```bash
        --== Initializing Snort ==--
Initializing Output Plugins!
Initializing Preprocessors!
Initializing Plug-ins!
Parsing Rules file "/etc/snort/snort.conf"
ERROR: /etc/snort/snort.conf(9) Failed to parse the IP address: !HOME_NET.
Fatal Error, Quitting..
```

### Passo 22
Outro alteração necessária por causa da atual sintaxe do snort. Dessa vez, a alteração deve ser feita em `local.rules`. Atualmente, cada regra deve ter um identificador com valores superiores a `1000000`, portanto é necessário fazer a seguinte alteração:

```diff
--- /etc/snort/rules/local.rules	2022-12-16 22:42:06.162969569 +0000
+++ /shared/local.rules	2022-12-15 16:47:10.648548391 +0000
@@ -4,8 +4,8 @@
 # ----------------
 # This file intentionally does not come with signatures.  Put your local
 # additions here.
-# alert tcp any any -> $HOME_NET 21 \
-(content:"anonymous";msg:"Login FTP (anonimo) invalido!";)
+alert tcp any any -> $HOME_NET 21 \
+(content:"anonymous";msg:"Login FTP (anonimo) invalido!";sid:1000001;)
 
 alert tcp $EXTERNAL_NET any -> $HOME_NET 21 \
-(msg:"Login de FTP externo";)
+(msg:"Login de FTP externo";sid:1000002)
```

### Passo 24
Para encerrar o lab, basta se desconectar das máquinas com:

```bash
exit
```

E depois executar:

```bash
kathara lclean
```
