# Colinha

```bash
# Passo 3
kathara lstart

# Passo 4
/etc/init.d/proftpd start
# ou
service proftpd start

# Passo 5
tcpdump -i eth0 -s 1024 -w /hosthome/lab16_ftp.pcap

# Passo 6
iptables -t nat -A PREROUTING -p tcp --dport 21 -j DNAT --to 10.1.1.13

# Passo 7
nmap -sX -A -v 10.1.1.1
nmap -sX -A -v 202.135.187.131

# Passo 8
ftp -n 202.135.187.131
# ftp> user manuel 123man

# Passo 9
# ftp> quit

# Passo 10
cp /root/snort_lab.conf /etc/snort/snort.conf

# Passo 11
var HOME_NET 0.0.0.0/0
var EXTERNAL_NET 0.0.0.0/0

# Passo 12
snort -vde

# Passo 13
# Refazer Passo 7

# Passo 14
# Ctrl-C
ping 10.1.1.1

# Passo 15
snort -A full -c /etc/snort/snort.conf

# Passo 16
# Refazer Passo 7

# Passo 17
cat /var/log/snort/alert

# Passo 19
# Ctrl-C
ping 10.1.1.1

# Passo 20
var HOME_NET 10.0.0.0/8
var EXTERNAL_NET !$HOME_NET

# Passo 21
# Refazer Passos 15 e depois 7

# Passo 22
alert tcp any any -> $HOME_NET 21 \
(content:"anonymous";msg:"Login FTP (anonimo) invalido!";sid:1000001;)
 
alert tcp $EXTERNAL_NET any -> $HOME_NET 21 \
(msg:"Login de FTP externo";sid:1000002)

# Passo 23
# Refazer Passos 15, 7 e 17

# Passo 24
exit
kathara lclean
```
