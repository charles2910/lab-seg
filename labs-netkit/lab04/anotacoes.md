# Adaptação e Execução do Laboratório 4 - Roteamento dinâmico para o Kathará

## Alterações Iniciais para Utilizar o Kathará

Apesar do kathará ter bastante compatibilidade com o netkit, algumas coisas
precisam ser alteradas. Por exemplo, o kathará não permite que os nomes das
máquinas sejam em letras maiúsculas, então é necessário alterar o arquivo
`lab.conf`, `lab.dep` e o nome das pastas que serão usadas pelas máquinas para
letras minúsculas - o diff entre o lab original e a versão atualizada pode ser
vista abaixo. Feitas essas alterações, já é possível iniciar o laboratório
utilizando o kathará (`kathara lstart`). Também adicionou-se a imagem docker
a ser usada por cada máquina.

```diff
TBD
```


## Alterações nos Passos do Roteiro

Como estamos usando o kathará e várias aplicações foram atualizadas, foi
necessário adaptar alguns passos do roteiro original. As alterações estão
indicadas abaixo pelo seu respectivo número no roteiro original.

### Objetivos do laboratório
Retirar trecho `Antes de baixar ... no tutorial de instalação.`.

### Passo 1
Alterar o nome do tar utilizado para `kathara_lab04.tar.gz` e trocar
`nklabs` por `labs-netkit`.

### Passo 2
Trocar o nome da pasta para `labs-netkit`.

### Passo 3
Alterar o nome do tar utilizado para `kathara_lab04.tar.gz` ao invés de
`netkit_lab04.tar.gz`. Substituir `nklabs` por `labs-netkit`.

### Passo 4
A inicialização do laboratório pelo kathará é realizado com o seguinte comando:

```bash
kathara lstart -d /home/seu_nome/labs-netkit/lab04
```

E remover trecho `sendo a principal apenas ... Virtual Console #1`.

### Passo 7
Adicionar ponto final em `ele irá responder a partir do` para ficar `ele
irá responder. A partir do`.

### Passos 8 e 19
O kathara permite opcionalmente a conexão com a máquina virtualizada através
do comando `kathara connect` - funciona da mesma forma que uma conexão ssh.
A vantagem é que permite usar o shell padrão do(a) usuário(a) e facilita a
visualização. É preciso de uma janela do terminal para cada comando.

```bash
kathara connect hostX
```

Ademais os comandos `ifconfig` e `route` estão obsoletos. Ele ainda vem
instalado por padrão no Debian 10, mas não deve vir no Debian 11. Toda a suíte
`net-tools`, `arp`, `ifconfig`, `route`, `netstat`, `ipmaddr`, ... está
obsoleta. Então deve-se pensar em migrar os roteiros para a ferramenta
`iproute2` que tem uma sintaxe parecida, mas não igual.

#### Tabela de equivalência
| Program   | Obsoleted by |
|-----------|--------------|
| arp       | ip neighbor  |
| ifconfig  | ip address   |
| ipmaddr   | ip maddress  |
| iptunnel  | ip tunnel    |
| route     | ip route     |
| nameif    | ifrename     |
| mii-tool  | ethtool      |

Portanto podemos executar `ip address` ao invés de `ifconfig` e `ip route` ao
invés de `route`.

### Passos 10, 11, 12, 13 e 14
Trocar `Virtual Console #1` por `terminal`.

### Passo 15
Trocar `daemon zebra` por `daemon frr` e o comando `/etc/init.d/zebra start`
por `/etc/init.d/frr start`.

### Passo 20
Trocar o nome do comando de `tracert` para `traceroute`.

### Passo 47 (extra)
Para encerrar o lab, basta se desconectar das máquinas com:

```bash
exit
```

E depois executar:

```bash
kathara lclean -d /home/seu_nome/labs-netkit/lab04
```
