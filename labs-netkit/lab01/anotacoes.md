# Adaptação e Execução do Laboratório Uma rede simples conectada por um hub para o Kathará

## Alterações Iniciais para Utilizar o Kathará

Apesar do kathará ter bastante compatibilidade com o netkit, algumas coisas
precisam ser alteradas. Por exemplo, o kathará não permite que os nomes das
máquinas sejam em letras maiúsculas, então é necessário alterar o arquivo
`lab.conf`, `lab.dep` e o nome das pastas que serão usadas pelas máquinas para
letras minúsculas - o diff entre o lab original e a versão atualizada pode ser
vista abaixo. Feitas essas alterações, já é possível iniciar o laboratório
utilizando o kathará (`kathara lstart`). Também adicionou-se a imagem docker
a ser usada por cada máquina.

```diff
diff --git a/lab01/HOST1.startup b/lab01/host1.startup
similarity index 100%
rename from lab01/HOST1.startup
rename to lab01/host1.startup
diff --git a/lab01/HOST1/etc/network/interfaces b/lab01/host1/etc/network/interfaces
similarity index 100%
rename from lab01/HOST1/etc/network/interfaces
rename to lab01/host1/etc/network/interfaces
diff --git a/lab01/HOST2.startup b/lab01/host2.startup
similarity index 100%
rename from lab01/HOST2.startup
rename to lab01/host2.startup
diff --git a/lab01/lab.conf b/lab01/lab.conf
index afa895f..9538f32 100755
--- a/lab01/lab.conf
+++ b/lab01/lab.conf
@@ -1,28 +1,26 @@
 #Introducao ao LAB
 LAB_DESCRIPTION='Laboratorio I - Uma simples rede com hub'
-LAB_VERSION='1.0'
-LAB_AUTHOR='Paulo Henrique Moreira Gurgel'
+LAB_VERSION='2.0'
+LAB_AUTHOR='Paulo Henrique Moreira Gurgel e Carlos Henrique Lima Melara'
 LAB_EMAIL='paulogur@grad.icmc.usp.br'
 LAB_WEB='http://www.grad.icmc.usp.br/~paulogur/netkit.php'
 
-#Ligacao das maquinas nos dominios de colisao
-#Todas as maquinas terao interfaces (eth0) ligadas ao hub virtual (HUB1)
-HOST1[0]=HUB1
-HOST2[0]=HUB1
-HOST3[0]=HUB1
-HOST4[0]=HUB1
+# Ligação das máquinas nos domínios de colisão
+# Todas as máquinas terão interfaces (eth0) ligadas ao hub virtual (HUB1)
+host1[0]=HUB1
+host2[0]=HUB1
+host3[0]=HUB1
+host4[0]=HUB1
 
-#Definicao da quantidade de memoria disponivel para cada host. 
-#Este lab requer pouco mais de 128Mb de RAM para ser executado
-HOST1[mem]=48
-HOST2[mem]=32
-HOST3[mem]=32
-HOST4[mem]=32
+# Definição da quantidade de memória disponível para cada host.
+# Este lab requer pouco mais de 128Mb de RAM para ser executado
+host1[mem]=48
+host2[mem]=32
+host3[mem]=32
+host4[mem]=32
 
-#Cria os cowfiles na pasta temp e nunca mais teremos problemas com o perfil XD
-HOST1[filesystem]=/tmp/L1HOST1.disk
-HOST2[filesystem]=/tmp/L1HOST2.disk
-HOST3[filesystem]=/tmp/L1HOST3.disk
-HOST4[filesystem]=/tmp/L1HOST4.disk
-
-#Havera inicializacao paralela pela presenca do lab.dep
+# Definição da imagem docker usada pelas máquinas
+host1[image]="lsec/netkit-lsec:debian10"
+host2[image]="lsec/netkit-lsec:debian10"
+host3[image]="lsec/netkit-lsec:debian10"
+host4[image]="lsec/netkit-lsec:debian10"
diff --git a/lab01/lab.conf~ b/lab01/lab.conf~
deleted file mode 100755
index de4014a..0000000
--- a/lab01/lab.conf~
+++ /dev/null
@@ -1,28 +0,0 @@
-#Introducao ao LAB
-LAB_DESCRIPTION='Laboratorio I - Uma simples rede com hub'
-LAB_VERSION='1.0'
-LAB_AUTHOR='Paulo Henrique Moreira Gurgel'
-LAB_EMAIL='paulogur@grad.icmc.usp.br'
-LAB_WEB='http://www.grad.icmc.usp.br/~paulogur/netkit.php'
-
-#Ligacao das maquinas nos dominios de colisao
-#Todas as maquinas terao interfaces (eth0) ligadas ao hub virtual (HUB1)
-HOST1[0]=HUB1
-HOST2[0]=HUB1
-HOST3[0]=HUB1
-HOST4[0]=HUB1
-
-#Definicao da quantidade de memoria disponivel para cada host. 
-#Este lab requer pouco mais de 128Mb de RAM para ser executado
-HOST1[mem]=48
-HOST2[mem]=32
-HOST3[mem]=32
-HOST4[mem]=32
-
-#Cria os cowfiles na pasta temp e nunca mais teremos problemas com o perfil XD
-HOST1[filesystem]=//tmp//L1HOST1.disk
-HOST2[filesystem]=//tmp//L1HOST2.disk
-HOST3[filesystem]=//tmp//L1HOST3.disk
-HOST4[filesystem]=//tmp//L1HOST4.disk
-
-#Havera inicializacao paralela pela presenca do lab.dep
diff --git a/lab01/lab.dep b/lab01/lab.dep
index dcd0530..30366ed 100755
--- a/lab01/lab.dep
+++ b/lab01/lab.dep
@@ -1,3 +1,2 @@
-HOST3: HOST1
-HOST4: HOST2
-
+host3: host1
+host4: host2
```


## Alterações nos Passos do Roteiro

Como estamos usando o kathará e várias aplicações foram atualizadas, foi
necessário adaptar alguns passos do roteiro original. As alterações estão
indicadas abaixo pelo seu respectivo número no roteiro original.

### Passo 2
Pode ser interessante alterar o nome do tar utilizado.

### Passo 3
A inicialização do laboratório pelo kathará é realizado com o seguinte comando:

```bash
kathara lstart
```

Assumindo que o diretório atual é o que contém o laboratório - mais
especificamente, o arquivo lab.conf. Caso contrário, pode usar a opção `-d` e
especificar o caminho do diretório contendo o lab.

Portanto, seria interessante criar um novo passo anterior que mude para
o diretório do laboratório e então execute a inicialização do lab:

```bash
cd lab01
```

```bash
kathara lstart
```

### Passo 4
O comando `vlist` foi substituído por `kathara list`, contudo ele não
está funcionando completamente. É possível listar os containers em execução
com `sudo kathara list --all`.

Eu vou abrir um bug no repositório do kathará para averiguar a situação.

### Passos 5, 6, 7 e 13
O kathara permite opcionalmente a conexão com a máquina virtualizada através
do comando `kathara connect` - funciona da mesma forma que uma conexão ssh.
A vantagem é que permite usar o shell padrão do(a) usuário(a) e facilita a
visualização. É preciso de uma janela do terminal para cada comando.

```bash
kathara connect hostX
```

Ademais o comando `ifconfig` está obsoleto. Ele ainda vem instalado por
padrão no Debian 10, mas não deve vir no Debian 11. Toda a suíte `net-tools`,
`arp`, `ifconfig`, `route`, `netstat`, `ipmaddr`, ... está obsoleta. Então
deve-se pensar em migrar os roteiros para a ferramenta `iproute2` que tem
uma sintaxe parecida, mas não igual.

#### Tabela de equivalência
| Program   | Obsoleted by |
|-----------|--------------|
| arp       | ip neighbor  |
| ifconfig  | ip address   |
| ipmaddr   | ip maddress  |
| iptunnel  | ip tunnel    |
| route     | ip route     |
| nameif    | ifrename     |
| mii-tool  | ethtool      |

### Passo 8
Trocar `hosthome` por `shared`.

### Passo 14
Trocar referências a `hosthome` por `shared` e também trocar a
explicação.

### Passo 15 (extra)
Para encerrar o lab, basta se desconectar das máquinas com:

```bash
exit
```

E depois executar:

```bash
kathara lclean
```

Se não estiver no diretório do lab, é possível indicar o caminho com a
opção `-d`.
