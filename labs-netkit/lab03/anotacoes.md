# Adaptação e Execução do Laboratório 3 - Roteamento estático para o Kathará

## Alterações Iniciais para Utilizar o Kathará

Apesar do kathará ter bastante compatibilidade com o netkit, algumas coisas
precisam ser alteradas. Por exemplo, o kathará não permite que os nomes das
máquinas sejam em letras maiúsculas, então é necessário alterar o arquivo
`lab.conf`, `lab.dep` e o nome das pastas que serão usadas pelas máquinas para
letras minúsculas - o diff entre o lab original e a versão atualizada pode ser
vista abaixo. Feitas essas alterações, já é possível iniciar o laboratório
utilizando o kathará (`kathara lstart`). Também adicionou-se a imagem docker
a ser usada por cada máquina.

```diff
diff --git a/lab03/HOST1.startup b/lab03/host1.startup
similarity index 100%
rename from lab03/HOST1.startup
rename to lab03/host1.startup
diff --git a/lab03/HOST2.startup b/lab03/host2.startup
similarity index 100%
rename from lab03/HOST2.startup
rename to lab03/host2.startup
diff --git a/lab03/HOST3.startup b/lab03/host3.startup
similarity index 100%
rename from lab03/HOST3.startup
rename to lab03/host3.startup
diff --git a/lab03/HOST4.startup b/lab03/host4.startup
similarity index 100%
rename from lab03/HOST4.startup
rename to lab03/host4.startup
diff --git a/lab03/lab.conf b/lab03/lab.conf
index 3bc8331..81cd3c7 100755
--- a/lab03/lab.conf
+++ b/lab03/lab.conf
@@ -8,35 +8,35 @@ LAB_WEB='https://www.lsec.icmc.usp.br/livronetkitbr/'
 #Ligacao das maquinas nos dominios de colisao
 #Duas pequenas redes interligadas pelo backbone
 
-ROTEADOR1[0]=BBONE
-ROTEADOR1[1]=ROT1A
-ROTEADOR1[2]=ROT1B
+roteador1[0]=BBONE
+roteador1[1]=ROT1A
+roteador1[2]=ROT1B
 
-ROTEADOR2[0]=BBONE
-ROTEADOR2[1]=HUB2
+roteador2[0]=BBONE
+roteador2[1]=HUB2
 
-HOST1[0]=ROT1A
-HOST2[0]=ROT1B
+host1[0]=ROT1A
+host2[0]=ROT1B
 
-HOST3[0]=HUB2
-HOST4[0]=HUB2
+host3[0]=HUB2
+host4[0]=HUB2
 
 
 #Definicao da quantidade de memoria disponivel para cada host. 
 #Este lab requer pouco mais de 256Mb de RAM para ser executado
-HOST1[mem]=32
-HOST2[mem]=32
-HOST3[mem]=32
-HOST4[mem]=32
-ROTEADOR1[mem]=64
-ROTEADOR2[mem]=64
+host1[mem]=32
+host2[mem]=32
+host3[mem]=32
+host4[mem]=32
+roteador1[mem]=64
+roteador2[mem]=64
 
 #Configuração dos filesystems
-HOST1[filesystem]=/tmp/L3H1.disk
-HOST2[filesystem]=/tmp/L3H2.disk
-HOST3[filesystem]=/tmp/L3H3.disk
-HOST4[filesystem]=/tmp/L3H4.disk
-ROTEADOR1[filesystem]=/tmp/L3R1.disk
-ROTEADOR2[filesystem]=/tmp/L3R2.disk
+host1[image]="icmclsec/netkit-lsec:debian10"
+host2[image]="icmclsec/netkit-lsec:debian10"
+host3[image]="icmclsec/netkit-lsec:debian10"
+host4[image]="icmclsec/netkit-lsec:debian10"
+roteador1[image]="icmclsec/netkit-lsec:debian10"
+roteador2[image]="icmclsec/netkit-lsec:debian10"
 
 #Havera inicializacao paralela pela presenca do lab.dep
diff --git a/lab03/lab.dep b/lab03/lab.dep
index d02d107..5fc9832 100755
--- a/lab03/lab.dep
+++ b/lab03/lab.dep
@@ -1,5 +1,5 @@
-HOST1: ROTEADOR1
-HOST2: ROTEADOR1
-HOST3: ROTEADOR2
-HOST4: ROTEADOR2
+host1: roteador1
+host2: roteador1
+host3: roteador2
+host4: roteador2
 
diff --git a/lab03/ROTEADOR1.startup b/lab03/roteador1.startup
similarity index 100%
rename from lab03/ROTEADOR1.startup
rename to lab03/roteador1.startup
diff --git a/lab03/ROTEADOR2.startup b/lab03/roteador2.startup
similarity index 100%
rename from lab03/ROTEADOR2.startup
rename to lab03/roteador2.startup
```


## Alterações nos Passos do Roteiro

Como estamos usando o kathará e várias aplicações foram atualizadas, foi
necessário adaptar alguns passos do roteiro original. As alterações estão
indicadas abaixo pelo seu respectivo número no roteiro original.

### Passo 1
Alterar o nome do tar utilizado para `kathara_lab03.tar.gz`.

### Passo 2
Trocar o nome da pasta para `labs-netkit`.

### Passo 3
Alterar o nome do tar utilizado para `kathara_lab03.tar.gz` ao invés de
`netkit_lab03.tar.gz`. Trocar `pasta lab03pronto` por `pasta lab03` e
substituir nklabs por labs-netkit.

### Passo 4
A inicialização do laboratório pelo kathará é realizado com o seguinte comando:

```bash
kathara lstart -d /home/seu_nome/labs-netkit/lab03
```

### Passo 5
O comando `vlist` foi substituído por `kathara list`, contudo ele não
está funcionando completamente. É possível listar os containers em execução
com `sudo kathara list --all`.

Eu vou abrir um bug no repositório do kathará para averiguar a situação.

### Passos 5, 6, 7, 9, 11, 13, 14, 15, 22
O kathara permite opcionalmente a conexão com a máquina virtualizada através
do comando `kathara connect` - funciona da mesma forma que uma conexão ssh.
A vantagem é que permite usar o shell padrão do(a) usuário(a) e facilita a
visualização. É preciso de uma janela do terminal para cada comando.

```bash
kathara connect hostX
```

Ademais os comandos `ifconfig` e `route` estão obsoletos. Ele ainda vem
instalado por padrão no Debian 10, mas não deve vir no Debian 11. Toda a suíte
`net-tools`, `arp`, `ifconfig`, `route`, `netstat`, `ipmaddr`, ... está
obsoleta. Então deve-se pensar em migrar os roteiros para a ferramenta
`iproute2` que tem uma sintaxe parecida, mas não igual.

#### Tabela de equivalência
| Program   | Obsoleted by |
|-----------|--------------|
| arp       | ip neighbor  |
| ifconfig  | ip address   |
| ipmaddr   | ip maddress  |
| iptunnel  | ip tunnel    |
| route     | ip route     |
| nameif    | ifrename     |
| mii-tool  | ethtool      |

Portanto podemos executar `ip address` ao invés de `ifconfig` e `ip route` ao
invés de `route`.

### Passo 10
Trocar `network unreached` por `Network is unreachable`.

### Passo 11
Corrigir concordância para `rota específica serão encaminhadas`.

### Passo 16, 17 e 18
Trocar `hosthome` por `shared` e adicionar flag `-v` após o comando `tcpdump`
para mostrar o número de pacotes capturados para os alunos.

### Passo 24 (extra)
Para encerrar o lab, basta se desconectar das máquinas com:

```bash
exit
```

E depois executar:

```bash
kathara lclean
```

Se não estiver no diretório do lab, é possível indicar o caminho com a
opção `-d`.

### Aprendendo um pouco mais sobre linux
Trocar `lclean` por `kathara lclean` e `máquina virtual` por `container`.
