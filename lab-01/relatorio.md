# Laboratório I – Uso da Ferramenta Nmap - Relatório
## Alunos
- Carlos Henrique Lima Melara 9805380
- Caio Marcos Chaves Viana 11208217

## Introdução
O objetivo deste laboratório é explorar a ferramenta nmap. Para isso,
foi necessário entender seu funcionamento, desde a funcionalidade básica
até a mais avançada - passando flags e opções para o programa. Durante
essa investigação, foram feitas [anotações](anotacoes.md) para sintetizar
as principais informações e gravar exemplos de uso. Após essa pesquisa,
foi feita uma investigação da rede do icmc/usp usando a ferramenta. Esta
será a parte detalhada nesse relatório.

## Reconhecimento
Duas abordagens iniciais de reconhecimento foram utilizadas. A primeira
foi um reconhecimento de endereços utilizando o site [dnsdumpster](https://dnsdumpster.com),
o qual utiliza OSInt para enumerar os endereços e ip's, e a segunda foi
um escaneamento de hosts na rede interna da usp.

As informações pertinentes fornecidas pelo dnsdumpster podem ser encontradas
no arquivo [icmc.usp.br-202209122009.xlsx](icmc.usp.br-202209122009.xlsx). Uma
[lista com os ip's resolvidos por records tipo A](dns_recon_icmc_usp) foi
gerada para realizar o escaneamento com o nmap (opção `-iL`). O resultado do
escaneamento das 30 portas mais populares (torna o escaneamento mais rápido
com efetividade de [aproximadamente 65%](https://nmap.org/book/performance-port-selection.html)) está no arquivo [dns_recon_scan_top_30_ports](dns_recon_scan_top_30_ports).

O escaneamento da rede interna do icmc/usp foi feito usando a opção `-sn`
na rede 172.26.0.0/16. O resultado parcial pode ser encontrado em [hosts_up](hosts_up).
Foi feito um escaneamento parcial nos hosts ligados entre os endereços
172.26.0.0/24 e 172.26.47.0/24 com a opção `-A`. Os resultados se encontram
no arquivo [partial_scan_hosts_up_till_47](partial_scan_hosts_up_till_47).

Além disso, foi feita uma varredura dos hosts encontrados de fora da rede do 
ICMC e o resultado está no arquivo [outside_dns_recon_scan_top_30_ports](outside_dns_recon_scan_top_30_ports) 
e será debatido na seção abaixo.

### Análise
Ao analisar as saídas do escaneamento nmap dos endereços do icmc revelou
potenciais problemas. Majoritariamente relacionados a SO e aplicações
desatualizadas que podem conter vulnerabilidades (casos mais comuns: apache,
exim, rpc, samba). Como exemplo, temos o host `repasse.icmc.usp.br`
rodando o apache versão 2.4.6 (lançado em 2013!) e este não é o único host
nessa situação! Outros achados interessantes foram MySQL exposto, vários
servidores RPC, repositório git exposto, wordpress, nextcloud exposto,
servidor ftp e dns.

Com relação ao escaneamento de hosts na rede do icmc/usp, tivemos somente
209 resultados positivos em 65535 ip's escaneados. Isto sugere que há algum
tipo de isolamento/firewall limitando o deslocamento horizontal pela rede (já
que certamente há muito mais do que 209 celulares conectados no eduroam
somente em São Carlos onde existem aprox. 10000 alunos).

Por fim, sobre a varredura feita de uma rede externa, podemos observar diversas 
portas que estavam com o status de `open`, se encontram como `filtered`, 
indicando que há um firewall de conexões vindas de fora da rede do instituto. 
Porém, ainda é possível enumerar alguns softwares utilizados, e identificar que 
vários deles estão desatualizados, ou seja, um atacante mesmo que não estivessena rede do ICMC, 
poderia ter diversas informações de forma totalmente remota. <br/>
Além disso, vale destacar que o host `gisa.icmc.usp.br` se encontra com uma porta ssh aberta
para a internet, algo totalmente desaconselhável e possível porta de entrada para diversas invasões.
